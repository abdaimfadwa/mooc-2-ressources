
 **TP1**:  Instructions de base en langage c

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau

    - Ressources didactique :orange_book: :langagec.pdf
    
    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme
    
    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )


- **TP2**:  introduction à la programmation avec c

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau
    
    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme

    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )
