
**MODULE** :algorithmique et programation
Séquence:langage de programmation
**Notions liées** :saisir,compiler,exécuter,traduire,boucle,structure,instructions 

**Résumé de l’activité** :afin de cet activité les apprenants sont invité à réalier un projet via un langage de programmation de leurs choix.

**Objectifs** :maîtrise la programmation et comprendre l'importance de la programmation dans la vie réelle.

**Auteur** :FADWA ABDAIM

**Durée de l’activité** :une heure (après la prise de connaissance du cours).

**Forme de participation** :groupe, sur machine.

**Matériel nécessaire** :ordinateur avec des éditeurs des langages choisis

**Préparation** :préparation du scénario pédagogique

**Fiche élève cours** :Disponible [ici](https://drive.google.com/file/d/1D3Z0maWb9P9HG9ih1t6bf4PA1HVm1R6p/view?usp=sharing)

**Fiche élève activité** :Disponible [ici](https://docs.google.com/presentation/d/1aiMMvtQM_aEw0RpMEMBWKCcf0kO5p3Ugs2u8BGAOFnU/edit?usp=sharing)
